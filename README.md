# OKCash telegram tipbot

This is a telegram bot that allows you to tip OKCash to people.

Username: [@okcash_bot](https://t.me/okcash_bot)

If you want to help development, clone the repo and setup your settings based off the config-sample.json file. Rename it to config.json
